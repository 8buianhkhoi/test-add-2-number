**Số lượng file**
Nộp gồm 2 file python có tên Exercise.py và test_Exercise.py. Đã viết 3 test case trong file test_Exercise.

**Chương trình dùng để làm gì**
File dùng thực thi phép cộng cho các số lớn hơn hoặc bằng 0.

**Cấu trúc file**
Tạo một class có tên MyBigNumber gồm 1 hàm constructor và 1 hàm sum để tính tổng.

**Cách sử dụng**
Khi sử dụng gọi tới class MyBigNumber.sum(para_1, para_2) để thực hiện.

**Ví dụ**
```
from Exercise import MyBigNumber

number_1 = input("Number 1: ")
number_2 = input("Number 2: ")

print(MyBigNumber.sum(number_1, number_2))

# Lưu ý : Chương trình trả về 1 dictionary gồm 2 cặp key-value, đầu tiền là cho kết quả phép cộng, thứ 2 là trình bày các bước làm chi tiết.
# Nếu muốn lấy kết quả thì hiển thị ra bằng cách :
print(MyBigNumber.sum(number_1, number_2)['result'])
#Hoặc muốn xem các bước thực hiện phép cộng :
print(MyBigNumber.sum(number_1, number_2)['detailStep'])
```
