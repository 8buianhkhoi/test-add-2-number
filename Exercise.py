# Number greater or equal 0
# Testing about add two larger number, use loop from right to left with each number.
# Name : Bui Anh Khoi
# Create class MyBigNumber and constructor function


# Create a class MyBigNumber and method sum. Method sum with 2 parameter are x and y, x is number 1, y is number 2.
class MyBigNumber:
    def __init__(self):
        pass

    @staticmethod
    def sum(x, y):
        resultStr = ""
        detailStep = ""
        stepCount = 1

        len_x = len(x)
        len_y = len(y)

        if (len_x < len_y):
            x, y = y, x
            len_x, len_y = len_y, len_x

        remain = 0

        for index in range(len_x-1, len_x - len_y -1, -1):
            resultPerLoop = int(x[index]) + int(y[len_y - 1]) + remain

            if remain == 0:
                strTempPlusRemain = "\n"
            else:
                strTempPlusRemain = f'. Cộng tiếp với nhớ {str(remain)} được {str(resultPerLoop)}\n'

            if resultPerLoop <= 9:
                resultStr = str(resultPerLoop) + resultStr
                remain = 0
            else:
                resultStr = str(resultPerLoop)[1] + resultStr
                remain = int(str(resultPerLoop)[0])

            detailStep = detailStep + f'Bước {str(stepCount)}: Lấy {x[index]} cộng với {y[len_y-1]} được {str(int(x[index]) + int(y[len_y - 1]))}' \
                         f'{strTempPlusRemain}Lưu {resultStr[0]} vào kết quả được kết quả mới là "{resultStr}"\nGhi nhớ {str(remain)}\n'

            stepCount = stepCount + 1
            len_y = len_y - 1
        len_y = len(y)

        for index in range(len_x - len_y - 1, -1, -1):
            resultPerLoop = int(x[index]) + remain

            if resultPerLoop <= 9:
                resultStr = x[0:index] + str(resultPerLoop) + resultStr
                remain = 0
                break
            else:
                resultStr = str(resultPerLoop)[1] + resultStr
                remain = int(str(resultPerLoop)[0])

            detailStep = detailStep + f'Bước {str(stepCount)}: Lấy {x[index]} cộng với {str(remain)} được {str(resultPerLoop)}' \
                         f'.Lưu {resultStr[0]} vào kết quả được kết quả mới là "{resultStr}"\nGhi nhớ {str(remain)}\n'

            stepCount = stepCount + 1

        if (remain != 0):
            resultStr = str(remain) + resultStr
            detailStep = detailStep + f'Bước {str(stepCount)}. Lưu {str(remain)} vào kết quả được kết quả mới là "{resultStr}"'

        return {'result':resultStr,'detailStep':detailStep}
    
