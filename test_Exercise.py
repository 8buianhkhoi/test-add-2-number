import unittest
from Exercise import MyBigNumber

class testExercise(unittest.TestCase):
    def test_add(self):
        result = MyBigNumber.sum('9999','9999')['result']
        self.assertEqual(result,'19998')
    def test_add_2(self):
        result = MyBigNumber.sum('0','0')['result']
        self.assertEqual(result,'0')
    def test_add_3(self):
        result = MyBigNumber.sum('12345888','123458889999')['result']
        self.assertEqual(result,'123471235887')

if __name__ == '__main__':
    unittest.main()
